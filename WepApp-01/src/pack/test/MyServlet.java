package pack.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MyServlet
 */
@WebServlet("/MyServlet")
public class MyServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private String name;
	private String email;
	private String ip;


    public MyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//*response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out = response.getWriter();
		response.getWriter().println(new Date());
		out.println("Hello Word this is MyServlet!!!....");
		
		name = request.getParameter("UserName");
		email = request.getParameter("UserEmail");
		ip = request.getRemoteAddr();
		
		response.getWriter().println("This employee user name:  " + name);
		response.getWriter().println("This employee user email:  " + email);
		response.getWriter().println("This employee user ip:  " + ip);
	}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
